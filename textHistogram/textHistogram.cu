#include <fstream>
#include <iostream>

using namespace std;

void fillTextHistogramCPU(char *fileData, int *histogram, size_t fileSize);
void writeTextHistogramCSV(int *histogram, ofstream *histFileHandle);
int main(int argc, char **argv)
{
	if(argc!=3)
	{
		printf("[ERR] Usage: %s textFile.txt histogramFile.txt\n",argv[0]);
		exit(0);
	}
	ifstream dataFileHandle;
	dataFileHandle.open(argv[1],ifstream::ate);
	if(!dataFileHandle.is_open())
	{
		printf("[ERR] Couldn't open file %s for reading\n",argv[1]);
		exit(0);
	}
	ofstream histFileHandle;
	histFileHandle.open(argv[2]);
	if(!histFileHandle.is_open())
	{
		printf("[ERR] Couldn't open file %s for writing\n",argv[2]);
		exit(0);
	}
	size_t fileSize = dataFileHandle.tellg();
	dataFileHandle.seekg(0,dataFileHandle.beg);
	printf("[INFO] File size: %lu bytes\n",fileSize);
	char *fileData;
	fileData = (char *)malloc(sizeof(char)*fileSize);
	dataFileHandle.read(fileData,fileSize);
	int bins = 'z'-'a'+1;
	int *histogram;
	histogram = (int *)malloc(sizeof(int)*bins);
	memset(histogram,0,sizeof(int)*bins);
	fillTextHistogramCPU(fileData,histogram,fileSize);
	writeTextHistogramCSV(histogram,&histFileHandle);
	dataFileHandle.close();
	histFileHandle.close();
	return 0;
}
void fillTextHistogramCPU(char *fileData, int *histogram, size_t fileSize)
{
	for(size_t idx=0;idx<fileSize;idx++)
	{
		char currentCharacter = tolower(fileData[idx]);
		if(currentCharacter>'z'|| currentCharacter<'a') continue;
		histogram[currentCharacter-'a']++;
	}
}

void writeTextHistogramCSV(int *histogram, ofstream *histFileHandle)
{
	*histFileHandle << "letter,freq" << endl;
	int binsNumber = 'z'-'a'+1;
	for(int i=0;i<binsNumber;i++)
		*histFileHandle << (char)('a'+i) << "," << histogram[i] << endl;
}
